//
//  Projectile.m
//  atatta
//
//  Created by Kevin Wong on 1/17/13.
//
//

#import "Projectile.h"

@implementation Projectile

-(void) delayNextHit
{
    isInactive = true;
    [self performSelector:@selector(reactivate) withObject:nil afterDelay:hitDelay];
}

-(void) reactivate
{
    isInactive = false;
}

@end