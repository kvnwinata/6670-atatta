//
//  Defender.h
//  atatta
//
//  Created by Kevin Wong on 1/17/13.
//
//

#import "CCSprite.h"

@interface Defender : CCSprite
{
    
    float recharge_period; // in seconds
    int health;

    int defenderSlotPosition; // 0 is bottom
    
    bool isAttacking;
    bool isRecharging;
    bool pauseAllowed;
    
    CCAction *animation;
    NSMutableArray *animationFrames;
    
}

-(id) initWithSlotPosition: (int) slot_position
           SpriteFrameName: (NSString*) frame_name;

-(bool) isTouchedAtPosition: (CGPoint) pos;

-(CGPoint) getCoordFromSlotPosition;

-(bool) isIncludingPoint: (CGPoint) location;

-(void) takeDamage:(int) damage;
-(void) updateSlotPosition:(int) slot;


-(void) recharge;
-(void) reactivate;


@end
