//
//  LaserBeeStoreItem.m
//  atatta
//
//  Created by Kevin Wong on 2/1/13.
//
//

#import "LaserBeeStoreItem.h"
#import "LaserBee.h"

@implementation LaserBeeStoreItem

-(id) init
{
    if ((self = [super initWithFile:@"laserbee-store.png"]))
	{
        // setup the variables:
        price = 500;
        
        [self setAnchorPoint:ccp(.5,.5)];
        [self setPosition:[self getCoordFromSlotPosition]];
        
        // resize the store item
        [self setScale:.25];
        
        [self scheduleUpdate];
	}
    return self;
}

-(Defender*) createDefenderAtSlot: (int) slot
{
    return [[LaserBee alloc] initWithSlotPosition:slot];
}


@end
