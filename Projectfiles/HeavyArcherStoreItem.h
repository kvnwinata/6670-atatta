//
//  HeavyArcherStoreItem.h
//  atatta
//
//  Created by Kevin Wong on 2/1/13.
//
//

#import "StoreItem.h"

@interface HeavyArcherStoreItem : StoreItem
-(id) init;
-(Defender*) createDefenderAtSlot: (int) slot;

@end
