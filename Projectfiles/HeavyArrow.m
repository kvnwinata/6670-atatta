//
//  HeavyArrow.m
//  atatta
//
//  Created by Kevin Wong on 2/1/13.
//
//

#import "HeavyArrow.h"

@implementation HeavyArrow

-(id) initAtPosition:(CGPoint)pos
            Velocity:(CGPoint)vel
{
    if (self = [super initWithFile:@"heavy-arrow.png"])
    {
        // setup properties:
        hitDelay = .05;
        maxHit = 3;
        damage = 20;
        
        [self setPosition: pos];
        [self setAnchorPoint:ccp(1,0.5)];
        [self setScale:0.1];
        velocity = vel;
        
        [self scheduleUpdate];
    }
    return self;
}


@end
