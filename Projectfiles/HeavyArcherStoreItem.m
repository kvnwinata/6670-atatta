//
//  HeavyArcherStoreItem.m
//  atatta
//
//  Created by Kevin Wong on 2/1/13.
//
//

#import "HeavyArcherStoreItem.h"
#import "HeavyArcher.h"

@implementation HeavyArcherStoreItem
-(id) init
{
    if ((self = [super initWithFile:@"heavyarcher-store.png"]))
	{
        // setup the variables:
        price = 250;
        
        [self setAnchorPoint:ccp(.5,.5)];
        [self setPosition:[self getCoordFromSlotPosition]];
        
        // resize the store item
        [self setScale:.25];
        
        [self scheduleUpdate];
	}
    return self;
}

-(Defender*) createDefenderAtSlot: (int) slot
{
    return [[HeavyArcher alloc] initWithSlotPosition:slot];
}

@end
