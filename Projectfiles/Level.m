//
//  Level.m
//  atatta
//
//  Created by Kevin Wong on 1/17/13.
//
//

#import "Level.h"
#import "LevelMenu.h"
#import "UtilityMethods.h"
#import "Defender.h"
#import "Enemy.h"
#import "SimpleAudioEngine.h"

@implementation Level

@synthesize level;
@synthesize enemiesManager;
@synthesize defendersManager;
@synthesize tutorialsManager;
@synthesize storeManager;
@synthesize projectilesManager;
@synthesize lastObjectTouched;

static Level * instanceOfCurrentLevel;

-(id) init
{
	if ((self = [super init]))
	{
        instanceOfCurrentLevel = self;
        
        // to prevent multiple sprites from detecting the same touch
        lastObjectTouched = 0;
        
        // initialize managers
        enemiesManager = [[EnemiesManager alloc] init];
        defendersManager = [[DefendersManager alloc] init];
        tutorialsManager = [[TutorialsManager alloc] init];
        storeManager = [[DefenderStoreManager alloc] init];
        projectilesManager = [[ProjectilesManager alloc] init];
        
        // setup background for level
        CCSprite * sprite;
        
        // sky
        sprite = [CCSprite spriteWithFile:@"sky.jpeg"];
        [sprite setAnchorPoint:ccp(0,0)];
        [sprite setPosition:ccp(0,0)];
        [self addChild:sprite z:-100];
        
        
        sprite = [CCSprite spriteWithFile:@"fence.png"];
        [sprite setAnchorPoint:ccp(0,0)];
        [sprite setScale:.5];
        [sprite setPosition:ccp(50,GROUND_HEIGHT-10)];
        [self addChild:sprite z:-99];
        
        sprite = [CCSprite spriteWithFile:@"fence.png"];
        [sprite setAnchorPoint:ccp(0,0)];
        [sprite setScale:.5];
        [sprite setPosition:ccp(0,GROUND_HEIGHT-10)];
        [self addChild:sprite z:-99];
        
        sprite = [CCSprite spriteWithFile:@"fence.png"];
        [sprite setAnchorPoint:ccp(0,0)];
        [sprite setScale:.5];
        [sprite setPosition:ccp(460,GROUND_HEIGHT-10)];
        [self addChild:sprite z:-99];
        
        // add managers/layers as children
        [self addChild:enemiesManager z:-1];
        
        // grass
        sprite = [CCSprite spriteWithFile:@"grass.png"];
        [sprite setAnchorPoint:ccp(0,0)];
        [sprite setPosition:ccp(0,0)];
        [sprite setScaleY:1.5];
        [self addChild:sprite z:0];
        
        line = [CCSprite spriteWithFile:@"line.png"];
        [line setScale:3];
        [self setAnchorPoint:CGPointZero];
        [self setPosition:ccp(0,0)];
        [self addChild:line];

        lineAnimation = [CCRepeatForever actionWithAction:[CCSequence actions:
                                                           [CCTintTo actionWithDuration:.5 red:80 green:80 blue:80],
                                                           [CCTintTo actionWithDuration:.5 red:255 green:255 blue:255],
                                                           nil]];
        lineAnimationRunning = false;
        
        [self addChild:defendersManager];
        [self addChild:tutorialsManager];
        [self addChild:projectilesManager];
        [self addChild:storeManager];
        
        levelNumber = [CCLabelTTF labelWithString:@""
                                         fontName:@"marker felt"
                                         fontSize:30];
        [levelNumber setAnchorPoint:ccp(.5,.5)];
        [levelNumber setColor:ccRED];
        [levelNumber setPosition:ccp(240, 160)];
        [levelNumber setVisible:NO];
        [self addChild:levelNumber];
        
        
        rumbleText = [CCSprite spriteWithFile:@"rumble.png"];
        [rumbleText setAnchorPoint:ccp(.5,.5)];
        [rumbleText setScale:.5];
        [rumbleText setPosition:ccp(240,160)];
        [self addChild:rumbleText];
        [rumbleText setVisible:NO];
        
        victoryText = [CCSprite spriteWithFile:@"victory.png"];
        [victoryText setAnchorPoint:ccp(.5,.5)];
        [victoryText setScale:.5];
        [victoryText setPosition:ccp(240,160)];
        [self addChild:victoryText];
        [victoryText setVisible:NO];
        
        defeatText = [CCSprite spriteWithFile:@"defeat.png"];
        [defeatText setAnchorPoint:ccp(.5,.5)];
        [defeatText setScale:.5];
        [defeatText setPosition:ccp(240,200)];
        [self addChild:defeatText];
        [defeatText setVisible:NO];
        
        
        
        CCMenuItemImage * reselectButton = [CCMenuItemImage itemWithNormalImage:@"select-level-button.png"
                                                                selectedImage:@"select-level-button-pressed.png"
                                                                       target:self
                                                                     selector:@selector(backToLevelSelection)];
        CCMenuItemImage * replayButton = [CCMenuItemImage itemWithNormalImage:@"replay-level.png"
                                                                  selectedImage:@"replay-level-pressed.png"
                                                                         target:self
                                                                       selector:@selector(replayLevelSelection)];
        [replayButton setAnchorPoint:ccp(.5,.5)];
        [replayButton setScale:.4];
        [replayButton setPosition:ccp(0,-50)];
        
        [reselectButton setAnchorPoint:ccp(.5,.5)];
        [reselectButton setScale:.4];
        [reselectButton setPosition:ccp(0,-100)];

        defeatMenu = [CCMenu menuWithItems:nil];
        [defeatMenu setPosition:ccp(240,160)];

        [defeatMenu addChild:replayButton];
        [defeatMenu addChild:reselectButton];
        
        // to fix display error in iphone5
        sprite = [CCSprite spriteWithFile:@"black.jpeg"];
        [sprite setAnchorPoint:ccp(0,0)];
        [sprite setPosition:ccp(480,0)];
        [self addChild:sprite z:100];
        
        [[SimpleAudioEngine sharedEngine] preloadBackgroundMusic:@"POL-boring-cavern-short.wav"];
        
        [[SimpleAudioEngine sharedEngine] preloadEffect:@"laser1.mp3"];
        [[SimpleAudioEngine sharedEngine] preloadEffect:@"laser3.mp3"];
        [[SimpleAudioEngine sharedEngine] preloadEffect:@"eat.m4a"];
        [[SimpleAudioEngine sharedEngine] preloadEffect:@"ketembak.aiff"];
        [[SimpleAudioEngine sharedEngine] preloadEffect:@"meledak.m4a"];
        [[SimpleAudioEngine sharedEngine] preloadEffect:@"arrow_lepas.aiff"];
        [[SimpleAudioEngine sharedEngine] preloadEffect:@"arrow_kena.aiff"];
        [[SimpleAudioEngine sharedEngine] preloadEffect:@"nembak.m4a"];
        [[SimpleAudioEngine sharedEngine] preloadEffect:@"gameover.mp3"];

	}
    return self;
}

-(void) resetLevel: (int) current_level
{
    level = current_level;
    [levelNumber setString:[NSString stringWithFormat:@"Level %d", current_level]];
    
    lastObjectTouched = 0;
    [self animateLine:false];
    
    // enable gestures
    KKInput * input = [KKInput sharedInput];
    input.gestureTapEnabled = YES;
    input.gesturePanEnabled = YES;
    input.gestureSwipeEnabled = YES;
    
    [projectilesManager resetManager];
    [enemiesManager resetManager];
    [defendersManager resetManager];
    [tutorialsManager resetManager];
 
    [self performSelector:@selector(showLevelNumber) withObject:nil afterDelay:.9];
}

-(void) showLevelNumber
{
    [levelNumber setVisible:YES];
    [self performSelector:@selector(showRumbleText) withObject:nil afterDelay:.8];
}

-(void) showRumbleText
{
    [levelNumber setVisible:NO];
    [rumbleText setVisible:YES];
    [self performSelector:@selector(beginLevel) withObject:nil afterDelay:.8];
}

-(void) beginLevel
{
    [rumbleText setVisible:NO];
    [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"POL-boring-cavern-short.wav" loop:YES];
    
    [storeManager resetManager];

    [tutorialsManager nextSegment];
}


+(id) sceneLevel:(int)level
{
    CCScene *scene = [CCScene node];
    
    if (instanceOfCurrentLevel == nil)
    {
        instanceOfCurrentLevel = [[Level alloc] init];
    }
    
    [instanceOfCurrentLevel resetLevel:level];
	[scene addChild: instanceOfCurrentLevel];
	return scene;
}

-(void) levelLost
{
    [projectilesManager clearManager];
    [enemiesManager clearManager];
    [defendersManager clearManager];
    [tutorialsManager clearManager];
    [storeManager clearManager];
    
    [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
    [[SimpleAudioEngine sharedEngine] playEffect:@"gameover.mp3"];
    
    [defeatText setVisible:YES];
    [self performSelector:@selector(levelLostPresentMenu) withObject:nil afterDelay:1.5];
}

-(void) levelLostPresentMenu
{
    // disable tap (not sure why this have to be done)
    KKInput * input = [KKInput sharedInput];
    input.gestureTapEnabled = NO;
    [self addChild:defeatMenu];
}

-(void) replayLevelSelection
{
    [self removeChild:defeatMenu cleanup:NO];
    [defeatText setVisible:NO];
    
    [[self parent] removeChild:self cleanup:NO];
    [[CCDirector sharedDirector] replaceScene:[Level sceneLevel:level]];
}

-(void) backToLevelSelection
{
    [self removeChild:defeatMenu cleanup:NO];
    [defeatText setVisible:NO];
    [[CCDirector sharedDirector] replaceScene:[LevelMenu scene]];
}

-(void) levelWon
{
    [[LevelMenu sharedLevelMenu] levelWon:level];
    
    [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
    [[SimpleAudioEngine sharedEngine] playEffect:@"fanfare7.wav"];
    [victoryText setVisible:YES];
    
    [self performSelector:@selector(levelWonEnd) withObject:nil afterDelay:4];
}

-(void) levelWonEnd
{
    [victoryText setVisible:NO];
    [projectilesManager clearManager];
    [enemiesManager clearManager];
    [defendersManager clearManager];
    [tutorialsManager clearManager];
    [storeManager clearManager];
    [[CCDirector sharedDirector] replaceScene:[LevelMenu scene]];    
}

-(void) activateEvent: (LevelEvent) event
{
    switch (event) {
        case victory:
            [self levelWon];
            break;
        case lost:
            [self levelLost];
            break;
        case spawn_wave:
            [[self enemiesManager] spawnNextWave];
            break;
        case next_tutorial:
            [[self tutorialsManager] nextSegment];
        default:
            break;
    }
}

+(Level*) sharedCurrentLevel
{
    return instanceOfCurrentLevel;
}

-(void) animateLine: (bool) animate
{
    if (animate)
    {
        if (!lineAnimationRunning){
            [line runAction:lineAnimation];
            lineAnimationRunning = true;
            [self performSelector:@selector(cancelLineAnimation) withObject:nil afterDelay:4];
        }
    } else {
        if (lineAnimationRunning)
        {
            [line stopAction:lineAnimation];
            [line setColor:ccWHITE];
            lineAnimationRunning = false;
        }
    }
}

-(void) cancelLineAnimation
{
    [self animateLine:false];
}

@end
