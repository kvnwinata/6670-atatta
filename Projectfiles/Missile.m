//
//  Missile.m
//  atatta
//
//  Created by Kevin Wong on 2/1/13.
//
//

#import "Missile.h"
#import "UtilityMethods.h"
#import "Level.h"
#import "SimpleAudioEngine.h"
#import "Enemy.h"

#define MAX_VELOCITY 700

@implementation Missile

-(id) initAtPosition:(CGPoint)pos
            Velocity:(CGPoint)vel
{
    if (self = [super initWithFile:@"missile.png"])
    {
        // setup properties:
        hitDelay = .1;
        maxHit = 2;
        damage = 40;
        
        [self setPosition: pos];
        [self setAnchorPoint:ccp(.5,.5)];
        [self setScale:0.2];
        
        velocity = ccpNormalize(vel);
        [self setRotation:-ccpToAngle(velocity)*(180./M_PI)+50];

        acceleration = ccpMult(velocity,1000);
        velocity = ccpMult(velocity, -100);
        
        [self scheduleUpdate];
        
        explosion = [CCSprite spriteWithFile:@"explosion.png"];
        
        box = [explosion boundingBox];
        
        [explosion setAnchorPoint:ccp(.5,.5)];
        [explosion setVisible:NO];
        [self addChild:explosion];
    }
    return self;
}

-(void) update: (ccTime) dt;
{
    if (ccpLength(velocity) < MAX_VELOCITY)
    {
        velocity = ccpAdd(velocity, ccpMult(acceleration,dt));
    }
    
    CGPoint currentPosition = self.position;
    CGPoint newPosition = ccp(currentPosition.x + velocity.x*dt, currentPosition.y + velocity.y*dt);
    [self setPosition:newPosition];
    
    bool offScreen = currentPosition.y < GROUND_HEIGHT || currentPosition.x > SCREEN_WIDTH || currentPosition.x < 0 || currentPosition.y > SCREEN_HEIGHT;
    
    if (offScreen)
    {
        [self unscheduleUpdate];
        [self explode];
    }
    
    // check for hit condition
    for(Enemy * enemy in [[Level sharedCurrentLevel] enemiesManager].children)
    {
        if ([enemy isIncludingPoint:self.position])
        {
            [self unscheduleUpdate];
            [self explode];
        }
    }
}

-(void) explode
{
    [[SimpleAudioEngine sharedEngine] playEffect:@"meledak.m4a"];
    [explosion setPositionRelativeToParentPosition:CGPointZero];
    
    [explosion setScale:1];
    box = [explosion boundingBox];
    [explosion setScale:0];
    [explosion setVisible:YES];
    
    [explosion runAction:[CCScaleTo actionWithDuration:.2 scale:3]];
    [explosion runAction:[CCRotateBy actionWithDuration:1.5 angle:45]];
    [explosion runAction:[CCScaleTo actionWithDuration:.1 scale:0]];
        
    [self performSelector:@selector(destroy) withObject:nil afterDelay:1.6];

}

-(void) destroy
{
    CCArray * enemies = [[Level sharedCurrentLevel] enemiesManager].children.copy;
    
    for(unsigned i = 0; i < enemies.count; i++)
    {
        Enemy * enemy = [enemies objectAtIndex:i];
        if ([self isIncluding:enemy])
        {
            [enemy takeDamage:damage];
            [[SimpleAudioEngine sharedEngine] playEffect:@"meledak.m4a"];
        }
    }
    
    [explosion runAction:[CCScaleTo actionWithDuration:.1 scale:0]];
    [self performSelector:@selector(removeSelf) withObject:nil afterDelay:.12];
}

-(void) removeSelf
{
    [[self parent] removeChild:self cleanup:YES];
}

-(bool) isIncluding:(Enemy*) enemy
{    
    return CGRectContainsPoint(box, ccpSub(enemy.position, self.position));
}

@end
