//
//  Rock.m
//  atatta
//
//  Created by Kevin Wong on 2/1/13.
//
//

#import "Rock.h"
#import "Level.h"
#import "SimpleAudioEngine.h"

@implementation Rock

-(id) initAtPosition:(CGPoint) pos
{
    if (self = [super initWithFile:@"rock.png"])
    {
        // setup properties:
        [self setPosition: pos];
        [self setAnchorPoint:ccp(.5,0.5)];
        [self setScale:0.1];
        [self scheduleUpdate];
    }
    return self;
}

-(void) update: (ccTime) dt
{
    CGPoint currentPosition = [self position];
    CGPoint newPosition = ccp(currentPosition.x + -100*dt, currentPosition.y);
    [self setPosition:newPosition];
    
    //check if hit a defender:
    for(Defender * defender in [[Level sharedCurrentLevel] defendersManager].children)
    {
        if ([defender isIncludingPoint:self.position])
        {            
            [defender takeDamage:5];
            [[SimpleAudioEngine sharedEngine] playEffect:@"ketembak.aiff"];
            
            [[self parent] removeChild:self cleanup:YES];
        }
    }

}






@end
