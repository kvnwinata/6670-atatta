//
//  MissileBee.m
//  atatta
//
//  Created by Kevin Wong on 2/1/13.
//
//

#import "MissileBee.h"
#import "Missile.h"
#import "Level.h"
#import "UtilityMethods.h"

#define MAX_SCALE .8

@implementation MissileBee

-(id) initWithSlotPosition: (int) slot_position
{
 	if ((self = [super initWithSlotPosition: slot_position
                            SpriteFrameName: @"redbee1.tiff"]))
	{
        
        // setup the variables:
        recharge_period = 15;
        health = 100;
        
        directionalArrow = [CCSprite spriteWithFile:@"directional-arrow.png"];
        [directionalArrow setAnchorPoint:ccp(0,0.5)];
        [directionalArrow setPositionRelativeToParentPosition:ccp(DEFENDER_SIZE, DEFENDER_SIZE)];
        [self addChild:directionalArrow z:10];
        [directionalArrow setVisible:NO];
        
        launcher = [CCSprite spriteWithFile:@"missile-launcher.png"];
        [launcher setAnchorPoint:ccp(.4,.6)];
        [launcher setScale:.9];
        [self addChild:launcher];
        [launcher setPositionRelativeToParentPosition:CGPointZero];
        
        // resize the yellowbee
        [self setScale:.4];
        
        // setup animation
        animationFrames = [NSMutableArray array];
        for(int i = 1; i <= 2; ++i)
        {
            [animationFrames addObject:
             [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName: [NSString stringWithFormat:@"redbee%d.tiff", i]]];
        }
        CCAnimation *animating = [CCAnimation animationWithFrames: animationFrames delay:.7f];
        animation = [CCRepeatForever actionWithAction: [CCAnimate actionWithAnimation:animating restoreOriginalFrame:NO]];
        [self runAction:animation];
        
        [self scheduleUpdate];
	}
    return self;
}


-(void) update: (ccTime) dt
{
    KKInput * input = [KKInput sharedInput];
    
    // currently or started panning
    if (input.gesturePanBegan)
    {// first time panning
        if (!isAttacking)
        {
            startLocation = input.gesturePanLocation;
            
            if ([self isTouchedAtPosition:startLocation])
            {
                
                if ([[Level sharedCurrentLevel] lastObjectTouched] == 0)
                {
                    //this defender is being touched
                    isAttacking = true;
                    //startLocation = self.position;
                    [[Level sharedCurrentLevel] setLastObjectTouched:self];
                    [directionalArrow setVisible:YES];
                    //[[[Level sharedCurrentLevel] projectilesManager] addChild:directionalArrow z:10];
                }
            }
            else
            {
                isAttacking = false;
            }
        }
    }
    else // pan ended
    {
        if (isAttacking)
        {
            // releasing attack
            startLocation = [directionalArrow convertToWorldSpace:directionalArrow.position];
            
            float angle = [UtilityMethods getAngleFromP1:startLocation toP2:endLocation] +90;
            
            Missile * missile = [[Missile alloc] initAtPosition:startLocation Velocity:ccpSub(endLocation, startLocation)];
            [[[Level sharedCurrentLevel] projectilesManager] addChild:missile z:0];
            [[SimpleAudioEngine sharedEngine] playEffect:@"nembak.m4a"];
            
            [directionalArrow setVisible:NO];
            [self recharge];
            [[Level sharedCurrentLevel] setLastObjectTouched:0];
        }
        isAttacking = false;
    }
    
    
    if (isAttacking){
        startLocation = [directionalArrow convertToWorldSpace:directionalArrow.position];
        
        endLocation = input.gesturePanLocation;
        [directionalArrow setRotation:[UtilityMethods getAngleFromP1:startLocation toP2:endLocation]+90];
        
        float newScale = [UtilityMethods getDistanceFromP1:startLocation toP2:endLocation]/([directionalArrow contentSize].width*self.scale);
        
        if (newScale > MAX_SCALE)
        {
            newScale = MAX_SCALE;
        }
        
        [directionalArrow setScale:newScale];
        
        [launcher setRotation:[UtilityMethods getAngleFromP1:startLocation toP2:endLocation]+90];
    }
}


@end
