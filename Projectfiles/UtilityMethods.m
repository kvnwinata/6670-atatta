//
//  UtilityMethods.m
//  atatta
//
//  Created by Kevin Wong on 1/17/13.
//
//

#import "UtilityMethods.h"

@implementation UtilityMethods

+(float) getAngleFromP1: (CGPoint) p1 toP2: (CGPoint) p2
{
    CGPoint vector = ccpSub(p1, p2);
    CGFloat radians = atan2f(vector.x, vector.y);
    CGFloat degrees = radians * (180. / M_PI);
    return degrees;
}

+(float) getDistanceFromP1: (CGPoint) p1 toP2: (CGPoint) p2
{
    CGPoint vector = ccpSub(p1, p2);
    return sqrtf(vector.x * vector.x + vector.y * vector.y);
}


@end
