//
//  DefendersManager.h
//  atatta
//
//  Created by Kevin Wong on 1/22/13.
//
//

#import "CCLayer.h"
#import "Defender.h"

@interface DefendersManager : CCLayer

@property int availableDefenderSlot;

-(void) resetManager;
-(void) clearManager;

-(bool) slot: (int) slot
 isTouchedAt: (CGPoint) pos;

-(void) addNewDefender: (Defender *) newDefender;

-(void) removeDefender: (Defender *) defender slot:(int) slotPosition;

@end
