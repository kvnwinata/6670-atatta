//
//  LaserBeam.h
//  atatta
//
//  Created by Kevin Wong on 2/1/13.
//
//

#import "Projectile.h"

@interface LaserBeam : Projectile

-(id) initWithPosition: (CGPoint) pos
              andAngle: (float) angle;


@end
