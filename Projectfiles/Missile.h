//
//  Missile.h
//  atatta
//
//  Created by Kevin Wong on 2/1/13.
//
//

#import "Projectile.h"

@interface Missile : Projectile
{
    CGPoint acceleration;
    CCSprite * explosion;
    CGRect box;
    
}
-(id) initAtPosition: (CGPoint) pos
            Velocity: (CGPoint) vel;

-(void) update: (ccTime) dt;


@end
