//
//  LevelMenu.h
//  atatta
//
//  Created by Kevin Wong on 1/16/13.
//
//

#import "CCLayer.h"

@interface LevelMenu : CCLayer
{
    CCMenu * myMenu;
    CCSprite * sky;
    CCSprite * selectLevelText;
    bool numberInitialized;
}

+(id) scene;
+(LevelMenu*) sharedLevelMenu;

-(void) levelWon: (int) level;

@end
