//
//  StoreItem.m
//  atatta
//
//  Created by Kevin Wong on 1/29/13.
//
//

#import "StoreItem.h"
#import "Level.h"
#import "DefenderStoreManager.h"

@implementation StoreItem

-(id) initWithSlotPosition: (int) slot_position
{
    
    itemSlotPosition = slot_position;
    availableDefenderSlot = -1;
    touched = false;
    purchasable = false;
    
    return [self init]; // init of subclasses
}

-(bool) isTouchedAtPosition: (CGPoint) pos
{
    int x = pos.x;
    int y = pos.y;
    
    if (y > 0 && y < GROUND_HEIGHT)
    {
        float slotCoord = x/(float)STORE_ITEM_WIDTH;
        
        if (itemSlotPosition < slotCoord && slotCoord < itemSlotPosition + 1)
        {
            return true;
        }
    }
    return false;
}

-(CGPoint) getCoordFromSlotPosition
{
    int x = (itemSlotPosition+0.5) * STORE_ITEM_WIDTH;
    int y = GROUND_HEIGHT/2;
    return ccp(x,y);
}

-(void) update: (ccTime) dt
{
    KKInput * input = [KKInput sharedInput];
    DefendersManager * defendersManager = [[Level sharedCurrentLevel] defendersManager];
    bool transactionSuccessful = false;
    
    if ([input gestureTapRecognizedThisFrame])
    {
        CGPoint pos = input.gestureTapLocation;
        
        if (!touched)
        {
            if([self isTouchedAtPosition:pos] && [[Level sharedCurrentLevel] lastObjectTouched] == 0)
            {
                if (purchasable)
                {
                    // this store item has just been tapped
                    availableDefenderSlot = [defendersManager availableDefenderSlot];
                    
                    if (availableDefenderSlot <= 3)
                    {
                        touched = true;
                        [self setColor:ccc3(150,150,250)]; // darken the button
                        [[Level sharedCurrentLevel] setLastObjectTouched:self];
                        [[[Level sharedCurrentLevel] storeManager] setPlacementGuideVisibility:YES];
                    }
                }
                else
                { // not purchasable
                    // play SFX
                }
            }
        }
    }
    
    // second touch right after the first one.
    if ([input anyTouchBeganThisFrame] && [[Level sharedCurrentLevel] lastObjectTouched] == self){
        // check if defender slot is touched
        CGPoint pos = [input locationOfAnyTouchInPhase:KKTouchPhaseAny];
        
        if ([defendersManager slot:availableDefenderSlot isTouchedAt:pos])
        {
            transactionSuccessful = true;
        }
        // nevertheless, reset the transaction
        touched = false;
        [self setColor:ccc3(255,255,255)];
        [[Level sharedCurrentLevel] setLastObjectTouched:0];
        [[[Level sharedCurrentLevel] storeManager] setPlacementGuideVisibility:NO];
    }
    
    if (transactionSuccessful)
    {
        // successful transaction:
        // decrement money
        [[[Level sharedCurrentLevel] storeManager] incrementMoney:-price];
        // add defender
        [defendersManager addNewDefender:[self createDefenderAtSlot:availableDefenderSlot]];
        
        if (input.gesturePanBegan)
        {
            // reset gesture
            input.gesturePanEnabled = NO;
            input.gesturePanEnabled = YES;
        }

    }
}

-(void) updatePurchasability: (int) current_money
{
    if (current_money >= price)
    {
        purchasable = true;
        [self setColor:ccc3(255,255,255)];
    } else {
        purchasable = false;
        [self setColor:ccc3(120,120,120)];
    }
}

-(void) updateAvailableDefenderSlot: (int) slot
{
    availableDefenderSlot = slot;
}

@end
