//
//  HeavyArrow.h
//  atatta
//
//  Created by Kevin Wong on 2/1/13.
//
//

#import "Arrow.h"

@interface HeavyArrow : Arrow

-(id) initAtPosition: (CGPoint) pos
            Velocity: (CGPoint) vel;

@end
