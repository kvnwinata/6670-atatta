//
//  NormalTurtle.m
//  atatta
//
//  Created by Kevin Wong on 1/30/13.
//
//

#import "NormalTurtle.h"

@implementation NormalTurtle

-(id) init
{
 	if ((self = [super initWithSpriteFrameName:@"turtle1.tiff"]))
	{
        health = 150;
        velocity = ccp(-20,0);
        moneyDrop = 20;

        attackingPosition = ccp(0,GROUND_HEIGHT+DEFENDER_SIZE/2);

        [self setScale:1];
        [self setAnchorPoint:ccp(0.5, .3)];
        [self setFlipX:YES];
        [self setPosition:ccp(480 + 100, GROUND_HEIGHT)];
        
        // setup animation
        animationFrames = [NSMutableArray array];
        for(int i = 1; i <= 4; ++i)
        {
            [animationFrames addObject:
             [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName: [NSString stringWithFormat:@"turtle%d.tiff", i]]];
        }
        CCAnimation *animating = [CCAnimation animationWithFrames: animationFrames delay:0.3f];
        animation = [CCRepeatForever actionWithAction: [CCAnimate actionWithAnimation:animating restoreOriginalFrame:NO]];
        [self runAction:animation];
        
        // schedule update: using superclass's update:
        [self scheduleUpdate];

	}
    return self;
}

@end
