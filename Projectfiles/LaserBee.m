//
//  LaserBee.m
//  atatta
//
//  Created by Kevin Wong on 2/1/13.
//
//

#import "LaserBee.h"
#import "Level.h"
#import "UtilityMethods.h"
#import "LaserBeam.h"

#define MAX_SCALE .4 

@implementation LaserBee

-(id) initWithSlotPosition: (int) slot_position
{
 	if ((self = [super initWithSlotPosition: slot_position
                            SpriteFrameName: @"wingbee1.tiff"]))
	{
        
        // setup the variables:
        recharge_period = 20;
        health = 100;
        
        directionalArrow = [CCSprite spriteWithFile:@"directional-arrow.png"];
        [directionalArrow setAnchorPoint:ccp(0,0.5)];
        [directionalArrow setPositionRelativeToParentPosition:ccp(DEFENDER_SIZE/2+5, DEFENDER_SIZE/2+10)];
        [self addChild:directionalArrow z:10];
        [directionalArrow setVisible:NO];
        
        laserGun = [CCSprite spriteWithFile:@"laser-gun.png"];
        [laserGun setAnchorPoint:ccp(-.1,.3)];
        [laserGun setScale:.3];
        [self addChild:laserGun];
        [laserGun setPositionRelativeToParentPosition:CGPointZero];
        
        
        // resize the yellowbee
        [self setScale:.8];
        
        // setup animation
        animationFrames = [NSMutableArray array];
        for(int i = 1; i <= 2; ++i)
        {
            [animationFrames addObject:
             [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName: [NSString stringWithFormat:@"wingbee%d.tiff", i]]];
        }
        CCAnimation *animating = [CCAnimation animationWithFrames: animationFrames delay:0.2f];
        animation = [CCRepeatForever actionWithAction: [CCAnimate actionWithAnimation:animating restoreOriginalFrame:NO]];
        [self runAction:animation];
        
        // schedule update: using superclass's update:
        [self scheduleUpdate];
	}
    return self;
}


-(void) update: (ccTime) dt
{
    KKInput * input = [KKInput sharedInput];
    
    // currently or started panning
    if (input.gesturePanBegan)
    {// first time panning
        if (!isAttacking)
        {
            startLocation = input.gesturePanLocation;
            
            if ([self isTouchedAtPosition:startLocation])
            {
                
                if ([[Level sharedCurrentLevel] lastObjectTouched] == 0)
                {
                    //this defender is being touched
                    isAttacking = true;
                    //startLocation = self.position;
                    [[Level sharedCurrentLevel] setLastObjectTouched:self];
                    [directionalArrow setVisible:YES];
                    [[SimpleAudioEngine sharedEngine] playEffect:@"laser3.mp3"];
                }
            }
            else
            {
                isAttacking = false;
            }
        }
    }
    else // pan ended
    {
        if (isAttacking)
        {
            // releasing attack
            startLocation = [directionalArrow convertToWorldSpace:directionalArrow.position];
            
            float angle = [UtilityMethods getAngleFromP1:startLocation toP2:endLocation] +90;
            
            LaserBeam * beam = [[LaserBeam alloc] initWithPosition:startLocation andAngle:angle];
            [[[Level sharedCurrentLevel] projectilesManager] addChild:beam z:0];

            [directionalArrow setVisible:NO];
            //[[[Level sharedCurrentLevel] projectilesManager] removeChild:directionalArrow cleanup:NO];
            [self recharge];
            [[Level sharedCurrentLevel] setLastObjectTouched:0];
            
            [[SimpleAudioEngine sharedEngine] playEffect:@"laser1.mp3"];
        }
        isAttacking = false;
    }
    
    
    if (isAttacking){
        startLocation = [directionalArrow convertToWorldSpace:directionalArrow.position];
        
        endLocation = input.gesturePanLocation;
        [directionalArrow setRotation:[UtilityMethods getAngleFromP1:startLocation toP2:endLocation]+90];
        
        float newScale = [UtilityMethods getDistanceFromP1:startLocation toP2:endLocation]/([directionalArrow contentSize].width*self.scale);
        
        if (newScale > MAX_SCALE)
        {
            newScale = MAX_SCALE;
        }
        
        [directionalArrow setScale:newScale];
        
        [laserGun setRotation:[UtilityMethods getAngleFromP1:startLocation toP2:endLocation]+120];
    }
}


@end
