//
//  HeavyArcher.m
//  atatta
//
//  Created by Kevin Wong on 2/1/13.
//
//

#import "HeavyArcher.h"
#import "Level.h"
#import "HeavyArrow.h"
#import "UtilityMethods.h"


#define MAX_SCALE .3

@implementation HeavyArcher

-(id) initWithSlotPosition: (int) slot_position
{
 	if ((self = [super initWithSlotPosition: slot_position
                            SpriteFrameName: @"yellowbee1.tiff"]))
	{
        
        // setup the variables:
        recharge_period = 2;
        health = 100;
        
        directionalArrow = [CCSprite spriteWithFile:@"directional-arrow.png"];
        [directionalArrow setAnchorPoint:ccp(0,0.5)];
        [directionalArrow setPositionRelativeToParentPosition:ccp(DEFENDER_SIZE/2+5, DEFENDER_SIZE/2+10)];
        [self addChild:directionalArrow z:10];
        [directionalArrow setVisible:NO];
        
        bow = [CCSprite spriteWithFile:@"heavy-bow.png"];
        [bow setAnchorPoint:ccp(.3,.3)];
        [bow setScale:.6];
        [self addChild:bow];
        [bow setPositionRelativeToParentPosition:CGPointZero];
        
        
        // resize the yellowbee
        [self setScale:.7];
        
        // setup animation
        animationFrames = [NSMutableArray array];
        for(int i = 1; i <= 5; ++i)
        {
            [animationFrames addObject:
             [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName: [NSString stringWithFormat:@"yellowbee%d.tiff", i]]];
        }
        CCAnimation *animating = [CCAnimation animationWithFrames: animationFrames delay:0.05f];
        animation = [CCRepeatForever actionWithAction: [CCAnimate actionWithAnimation:animating restoreOriginalFrame:NO]];
        [self runAction:animation];
        
        [self scheduleUpdate];
	}
    return self;
}


-(void) update: (ccTime) dt
{
    KKInput * input = [KKInput sharedInput];
    
    // currently or started panning
    if (input.gesturePanBegan)
    {// first time panning
        if (!isAttacking)
        {
            startLocation = input.gesturePanLocation;
            
            if ([self isTouchedAtPosition:startLocation])
            {
                
                if ([[Level sharedCurrentLevel] lastObjectTouched] == 0)
                {
                    //this defender is being touched
                    isAttacking = true;
                    //startLocation = self.position;
                    [[Level sharedCurrentLevel] setLastObjectTouched:self];
                    [directionalArrow setVisible:YES];
                    //[[[Level sharedCurrentLevel] projectilesManager] addChild:directionalArrow z:10];
                }
            }
            else
            {
                isAttacking = false;
            }
        }
    }
    else // pan ended
    {
        if (isAttacking)
        {
            // releasing attack
            startLocation = [directionalArrow convertToWorldSpace:directionalArrow.position];
            
            Projectile * proj = [[HeavyArrow alloc] initAtPosition:startLocation Velocity:ccpMult(ccpSub(endLocation, startLocation),6)];
            [[[Level sharedCurrentLevel] projectilesManager] addChild:proj z:10];
            
            [directionalArrow setVisible:NO];
            
            [self recharge];
            [[Level sharedCurrentLevel] setLastObjectTouched:0];
        }
        isAttacking = false;
    }
    
    
    if (isAttacking){
        startLocation = [directionalArrow convertToWorldSpace:directionalArrow.position];
        
        endLocation = input.gesturePanLocation;
        [directionalArrow setRotation:[UtilityMethods getAngleFromP1:startLocation toP2:endLocation]+90];
        
        float newScale = [UtilityMethods getDistanceFromP1:startLocation toP2:endLocation]/([directionalArrow contentSize].width*self.scale);
        
        if (newScale > MAX_SCALE)
        {
            newScale = MAX_SCALE;
        }
        
        [directionalArrow setScale:newScale];
        
        [bow setRotation:[UtilityMethods getAngleFromP1:startLocation toP2:endLocation]+130];
    }
}


@end
