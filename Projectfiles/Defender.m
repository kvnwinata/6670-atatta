//
//  Defender.m
//  atatta
//
//  Created by Kevin Wong on 1/17/13.
//
//

#import "Level.h"
#import "Projectile.h"

#import "UtilityMethods.h"

@implementation Defender


-(id) initWithSlotPosition: (int) slot_position
           SpriteFrameName: (NSString*) frame_name;
{
 	if ((self = [super initWithSpriteFrameName:frame_name]))
    {
        defenderSlotPosition = slot_position;
        pauseAllowed = true;
        
        [self setAnchorPoint:ccp(.5,.5)];
        [self setPosition:[self getCoordFromSlotPosition]];
	}
    return self;
}

-(CGPoint) getCoordFromSlotPosition
{
    int x = DEFENDER_SIZE/2;
    int y = GROUND_HEIGHT + DEFENDER_SIZE* (0.5 + defenderSlotPosition);
    return ccp(x, y);
}

-(bool) isTouchedAtPosition: (CGPoint) pos
{
    int x = pos.x;
    int y = pos.y;
    
    if (x > 0 && x < DEFENDER_SIZE)
    {
        float defenderCoord = (y - GROUND_HEIGHT)/(float)DEFENDER_SIZE;
        
        if (defenderSlotPosition < defenderCoord && defenderCoord < defenderSlotPosition + 1)
        {
            return true;
        }
    }
    return false;
}

-(void) recharge
{
    // recharge logic;
    isRecharging = true;
    [self setColor:ccc3(80,80,80)];
    
    
    if (pauseAllowed)
    {
        [self pauseSchedulerAndActions];
    }
    
    [self performSelector:@selector(reactivate) withObject:nil afterDelay:recharge_period];
}

-(void) reactivate
{
    isRecharging = false;
    [self setColor:ccWHITE];
    [self resumeSchedulerAndActions];
}

-(bool) isIncludingPoint:(CGPoint)location
{
    return [self isTouchedAtPosition:location];
}

-(void) takeDamage:(int)damage
{
    health -= damage;
    [[Level sharedCurrentLevel] animateLine:true];
    
    [self setColor:ccRED];
    [self performSelector:@selector(resetColor) withObject:nil afterDelay:.5];
    
    if (health <= 0)
    {
        [self removeAllChildrenWithCleanup:YES];
        [[[Level sharedCurrentLevel] defendersManager] removeDefender:self slot:defenderSlotPosition];
    }
}

-(void) updateSlotPosition:(int) slot
{
    if (slot < defenderSlotPosition)
    {
        defenderSlotPosition --;
        [self resumeSchedulerAndActions];
        [self runAction:[CCJumpBy actionWithDuration:.3  position:ccp(0,-DEFENDER_SIZE) height:0 jumps:1]];
        pauseAllowed = false;
        [self performSelector:@selector(allowPausing) withObject:nil afterDelay:.3];
    }
}

-(void) allowPausing
{
    pauseAllowed = true;
}

-(void) resetColor
{
    [self setColor:ccWHITE];
}

@end
