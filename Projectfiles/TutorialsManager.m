//
//  TutorialsManager.m
//  atatta
//
//  Created by Kevin Wong on 1/22/13.
//
//

#import "TutorialsManager.h"
#import "Level.h"

@implementation TutorialsManager

-(id) init
{
    if (self = [super init])
    {
        nextEvent = 0;
        
        isWaitingTapText = false;
        // text holder
        textHolder = [CCSprite spriteWithFile:@"money-plank.jpeg"];
        [textHolder setAnchorPoint:ccp(.5,.5)];
        [textHolder setPosition:ccp(240,260)];
        [self addChild:textHolder];
        [textHolder setVisible:NO];
        
        // text
        text = [CCLabelTTF labelWithString: @""
                                dimensions: CGSizeMake(200, 100)
                                 alignment: CCTextAlignmentLeft
                                  fontName: @"marker felt"
                                  fontSize: 16];
        [text setAnchorPoint:ccp(.5,.5)];
        [text setColor:ccYELLOW];
        [text setPosition:ccp(240,240)];
        [self addChild:text];
        
        // bee
        bee = [CCSprite spriteWithSpriteFrameName:@"yellowbee1.tiff"];
        NSMutableArray * animationFrames = [NSMutableArray array];
        for(int i = 1; i <= 5; ++i)
        {
            [animationFrames addObject:
             [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName: [NSString stringWithFormat:@"yellowbee%d.tiff", i]]];
        }
        CCAnimation *animating = [CCAnimation animationWithFrames: animationFrames delay:0.05f];
        animation = [CCRepeatForever actionWithAction: [CCAnimate actionWithAnimation:animating restoreOriginalFrame:NO]];
        [bee setVisible:NO];
        [bee setScale:.5];
        [bee setPosition:ccp(350,220)];
        [self addChild:bee];
        
        arrow = [CCSprite spriteWithFile:@"tutorial-arrow.png"];
        [arrow setScale:.5];
        [arrow setAnchorPoint:ccp(.5,1)];
        [arrow setVisible:NO];
        [self addChild:arrow];
        
    }
    return self;
}

-(void) update: (ccTime) dt
{
    updateScheduled = true;
    bool activateEvent = false;
    
    if (isWaitingTapText)
    {
        KKInput * input = [KKInput sharedInput];
        
        if ([input gestureTapRecognizedThisFrame])
        {
            CGPoint pos = input.gestureTapLocation;
            if (CGRectContainsPoint([textHolder boundingBox], pos))
            {
                [text setString:@""];
                [textHolder setVisible:NO];
                [bee setVisible:NO];
                isWaitingTapText = false;
                activateEvent = true;
            }
        }
    }
    
    if (activateEvent)
    {
        [[Level sharedCurrentLevel] animateLine:false];
        [arrow setVisible:NO];
        
        if (nextEvent > 0)
        {
            [[Level sharedCurrentLevel] activateEvent:nextEvent];
        }
    }
    
}

-(void) resetManager
{
    updateScheduled = false;
    int currentLevel = [[Level sharedCurrentLevel] level];
    nextEvent = 0;
    currentSegment = 0;
    
    // load the tutorial
    NSString *path = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"tutorialLevel%i", currentLevel] ofType:@"plist"];
    tutorialSegments = [NSArray arrayWithContentsOfFile:path];
    
    if ([bee numberOfRunningActions] == 0)
    {
        [bee runAction:animation];
    }
    
}

-(void) rerunScheduledActions
{
    [self scheduleUpdate];
    [bee runAction:animation];
}

-(void) nextSegment
{
    nextEvent = 0;
    NSArray * segment = [tutorialSegments objectAtIndex:currentSegment];
    
    for (NSDictionary * item in segment)
    {
        if ([@"text" isEqualToString:[item objectForKey:@"type"]])
        {
            [text setString:[item objectForKey:@"content"]];
            [text setVisible:NO];
            [self performSelector:@selector(displayText) withObject:nil afterDelay:.2];
        }
        else if ([@"arrow" isEqualToString:[item objectForKey:@"type"]])
        {
            int angle = [[item objectForKey:@"angle"] integerValue];
            int x = [[item objectForKey:@"x"] integerValue];
            int y = [[item objectForKey:@"y"] integerValue];
            [arrow setRotation:angle];
            [arrow setPosition:ccp(x,y)];
            [arrow setVisible:YES];
        }
        else if ([@"line" isEqualToString:[item objectForKey:@"type"]])
        {
            [[Level sharedCurrentLevel] animateLine:true];
        }
        
        // event
        if ([item valueForKey:@"event"] != nil)
        {
            nextEvent =  (LevelEvent)[[item valueForKey:@"event"] integerValue];
        }
    }
    
    if (!updateScheduled)
    {
       [self scheduleUpdate];
    }
    
    currentSegment++;
}

-(void) displayText
{
    [text setVisible:YES];
    [textHolder setVisible:YES];
    [bee setVisible:YES];
    isWaitingTapText = true;
}

-(void) clearManager
{
}

@end
