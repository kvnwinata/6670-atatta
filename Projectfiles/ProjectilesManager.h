//
//  ProjectilesManager.h
//  atatta
//
//  Created by Kevin Wong on 1/23/13.
//
//

#import "CCLayer.h"

@interface ProjectilesManager : CCLayer

-(void) resetManager;
-(void) clearManager;

@end
