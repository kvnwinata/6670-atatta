//
//  MissileBeeStoreItem.m
//  atatta
//
//  Created by Kevin Wong on 2/1/13.
//
//

#import "MissileBeeStoreItem.h"
#import "MissileBee.h"

@implementation MissileBeeStoreItem

-(id) init
{
    if ((self = [super initWithFile:@"missilelauncher-store.png"]))
	{
        // setup the variables:
        price = 400;
        
        [self setAnchorPoint:ccp(.5,.5)];
        [self setPosition:[self getCoordFromSlotPosition]];
        
        // resize the store item
        [self setScale:.25];
        
        [self scheduleUpdate];
	}
    return self;
}

-(Defender*) createDefenderAtSlot: (int) slot
{
    return [[MissileBee alloc] initWithSlotPosition:slot];
}

@end
