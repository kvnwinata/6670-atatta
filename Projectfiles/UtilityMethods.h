//
//  UtilityMethods.h
//  atatta
//
//  Created by Kevin Wong on 1/17/13.
//
//

#import <Foundation/Foundation.h>

@interface UtilityMethods : NSObject

+(float) getAngleFromP1: (CGPoint) p1 toP2: (CGPoint) p2;
+(float) getDistanceFromP1: (CGPoint) p1 toP2: (CGPoint) p2;
@end
