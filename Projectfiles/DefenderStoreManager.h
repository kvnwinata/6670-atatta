//
//  DefenderStoreManager.h
//  atatta
//
//  Created by Kevin Wong on 1/22/13.
//
//

#import "CCLayer.h"

#define COMBO_RESET_TIME 1.5

@interface DefenderStoreManager : CCLayer
{
    int combo;
    int money;

    CCLabelTTF * comboText;
    CCSprite * comboPlank;
    
    CCLabelTTF * moneyText;
    
    CCSequence * comboAction;
    
    CCSprite * moneyPlank;
    CCSprite * honey;
    CCSprite * smallHoney;
    
    CCSprite * placementGuide;

}

-(void) resetManager;
-(void) clearManager;

-(void) incrementCombo;
-(void) incrementMoney: (int) amount;
-(void) incrementMoney: (int) amount position: (CGPoint) pos;


-(void) updateAvailableDefenderSlot: (int) slot;
-(void) updatePlacementGuide:(int) availableDefenderSlot;

-(void) setPlacementGuideVisibility: (bool) visibility;

@end
