//
//  EnemiesManager.h
//  atatta
//
//  Created by Kevin Wong on 1/22/13.
//
//

#import "CCLayer.h"

@interface EnemiesManager : CCLayer
{
    int currentWave;
    NSArray *enemiesWave;
}
-(void) resetManager;
-(void) clearManager;
-(void) spawnNextWave;

-(void) resetAttackingEnemies;
@end
