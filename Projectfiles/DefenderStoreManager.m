//
//  DefenderStoreManager.m
//  atatta
//
//  Created by Kevin Wong on 1/22/13.
//
//


#import "DefenderStoreManager.h"
#import "HeavyArcherStoreItem.h"
#import "MissileBeeStoreItem.h"
#import "ArcherStoreItem.h"
#import "LaserBeeStoreItem.h"
#import "Level.h"
#import "SimpleAudioEngine.h"

#define MAX_MONEY 9999

@implementation DefenderStoreManager

-(id) init
{
    if (self = [super init])
    {
        combo = 0;
        
        comboText = [CCLabelTTF labelWithString:@""
                                       fontName:@"marker felt"
                                       fontSize:30];
        [comboText setColor:ccYELLOW];
        [comboText setPosition:ccp(380, 260)];
        
        comboPlank = [CCSprite spriteWithFile:@"combo.png"];
        [comboPlank setAnchorPoint:ccp(0.8,0.5)];
        [comboPlank setScale:.25];
        [comboPlank setPosition:ccp(380, 260)];

        
        moneyText = [CCLabelTTF labelWithString:@""
                                       fontName:@"marker felt"
                                       fontSize:20];
        [moneyText setAnchorPoint:ccp(.5,.5)];
        [moneyText setColor:ccYELLOW];
        [moneyText setPosition:ccp(450, 300)];
                
        CCScaleTo * upscale   = [CCScaleTo actionWithDuration:.05 scale:2];
        CCScaleTo * downscale = [CCScaleTo actionWithDuration:.10 scale:1];

        comboAction = [CCSequence actions:upscale, downscale, nil];

        moneyPlank = [CCSprite spriteWithFile:@"money-plank.jpeg"];
        [moneyPlank setAnchorPoint:ccp(.5,.5)];
        [moneyPlank setPosition:ccp(440,300)];
        [moneyPlank setScale:.3];
        
        honey = [CCSprite spriteWithFile:@"honey.png"];
        [honey setAnchorPoint:ccp(.5,.5)];
        [honey setPosition:ccp(400, 300)];
        [honey setScale:.15];
        
        smallHoney = [CCSprite spriteWithFile:@"honey.png"];
        [smallHoney setAnchorPoint:ccp(.5,.5)];
        [smallHoney setScale:.05];
        [smallHoney setPosition:ccp(-10,-10)];
        
        placementGuide = [CCSprite spriteWithFile:@"guide.png"];
        [placementGuide setScale:.4];
        [placementGuide setAnchorPoint:ccp(.5,.5)];
        [placementGuide setVisible:NO];
        
    }
    return self;
}

-(void) resetManager
{
    
    int currentLevel = [[Level sharedCurrentLevel] level];

    
    combo = 0;
    [comboPlank setVisible:NO];
    [comboText setString:@""];
    [self addChild:comboText z:12];
    [self addChild:comboPlank z:10];
    [comboPlank setVisible:FALSE];
    [self addChild:moneyText z:12];
    [self addChild:moneyPlank z:10];
    [self addChild:honey z:11];
    [self addChild:smallHoney z:20];

    [self addChild:placementGuide];
    [self updatePlacementGuide:0];
    
    money = 0;
    [self incrementMoney:300];
        
    [self addChild:[[ArcherStoreItem alloc] initWithSlotPosition:0]];
    if (currentLevel >= 2)
    {
        [self addChild:[[HeavyArcherStoreItem alloc] initWithSlotPosition:1]];
    }
    if (currentLevel >= 3)
    {
        [self addChild:[[MissileBeeStoreItem alloc] initWithSlotPosition:2]];
        [self addChild:[[LaserBeeStoreItem alloc] initWithSlotPosition:3]];
    }
    
    [self updateItemsPurchasability];
}

-(void) clearManager
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [self removeAllChildrenWithCleanup:YES];
}


-(void) incrementCombo
{
    combo++;
    if (combo >= 2)
    {
        [comboPlank setVisible:YES];
        [comboText setString: [NSString stringWithFormat:@"x%d",combo]];
        [comboText stopAllActions];
        [comboText runAction:comboAction];
    }
    
    // reset the combo after a certain period of time:
    [NSObject cancelPreviousPerformRequestsWithTarget:self]; // removes the previous combo resetter
    [self performSelector:@selector(resetCombo) withObject:nil afterDelay:COMBO_RESET_TIME];
}

-(void) resetCombo
{
    int comboMoney = 5*combo*(1+combo/5);
    if (combo > 1){
        [self incrementMoney:comboMoney position:comboText.position];
    }
    combo = 0;
    [comboPlank setVisible:NO];
    [comboText setString:@""];
}

-(void) incrementMoney: (int) amount
{
    money += amount;
    
    if (money > MAX_MONEY)
    {
        money = MAX_MONEY;
    }
    
    [moneyText setString:[NSString stringWithFormat:@"%d", money]];
    
    [self updateItemsPurchasability];
}

-(void) incrementMoney: (int) amount position: (CGPoint) pos
{
    [smallHoney setPosition:pos];
    [smallHoney runAction:[CCSequence actions:
                           [CCMoveTo actionWithDuration:.2 position:honey.position],
                           [CCMoveTo actionWithDuration:0 position:ccp(-10,-10)],
                           nil]];
    [[SimpleAudioEngine sharedEngine] playEffect:@"coin.aiff" pitch:2 pan:1 gain:1];
    
    [self incrementMoney:amount];
}


-(void) updateItemsPurchasability
{
    for (StoreItem * item in [self children])
    {
        if ([item isKindOfClass:[StoreItem class]])
        {
            [item updatePurchasability:money];   
        }
    }
}

-(void) updateAvailableDefenderSlot: (int) slot
{
    for (StoreItem * item in [self children])
    {
        if ([item isKindOfClass:[StoreItem class]])
        {
            [item updateAvailableDefenderSlot:slot];
        }
    }
}

-(void) updatePlacementGuide:(int) availableDefenderSlot
{
    [placementGuide setPosition:ccp(DEFENDER_SIZE/2, GROUND_HEIGHT+DEFENDER_SIZE*(.5+availableDefenderSlot))];
}

-(void) setPlacementGuideVisibility: (bool) visibility
{
    [placementGuide setVisible:visibility];
}


@end
