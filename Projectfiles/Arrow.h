//
//  Arrow.h
//  atatta
//
//  Created by Kevin Wong on 1/24/13.
//
//

#import "Projectile.h"

@interface Arrow : Projectile

-(id) initAtPosition: (CGPoint) pos
            Velocity: (CGPoint) vel;

-(void) update: (ccTime) dt;

@end
