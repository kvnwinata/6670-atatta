//
//  Arrow.m
//  atatta
//
//  Created by Kevin Wong on 1/24/13.
//
//

#import "Arrow.h"
#import "UtilityMethods.h"
#import "Level.h"
#import "SimpleAudioEngine.h"
#import "Enemy.h"

#define g 600

@implementation Arrow

-(id) initAtPosition:(CGPoint)pos
            Velocity:(CGPoint)vel
{
    if (self = [super initWithFile:@"arrow.png"])
    {
        // setup properties:
        hitDelay = 0;
        maxHit = 1;
        damage = 20;
        
        [self setPosition: pos];
        [self setAnchorPoint:ccp(1,0.5)];
        [self setScale:0.1];
        velocity = vel;
        
        [self scheduleUpdate];
        
        [[SimpleAudioEngine sharedEngine] playEffect:@"arrow_lepas.aiff"];

    }
    return self;
}

-(void) update: (ccTime) dt;
{
    velocity.y = velocity.y - g*dt;
    
    [self setRotation:-ccpToAngle(velocity)*(180./M_PI)+160];
    
    CGPoint currentPosition = self.position;
    CGPoint newPosition = ccp(currentPosition.x + velocity.x*dt, currentPosition.y + velocity.y*dt);
    [self setPosition:newPosition];
    
    if ([self position].y < GROUND_HEIGHT || [self position].x > SCREEN_WIDTH)
    {
        [[self parent] removeChild:self cleanup:YES];
    }
    
    if (isInactive) return;

    // check for hit condition
    for(Enemy * enemy in [[Level sharedCurrentLevel] enemiesManager].children)
    {
        if ([enemy isIncludingPoint:self.position])
        {
            if (maxHit > 0){
                maxHit -= 1;
                [enemy takeDamage:damage];
                [[SimpleAudioEngine sharedEngine] playEffect:@"arrow_kena.aiff"];
                [self delayNextHit];
            }
        }
    }
    
    
    if (maxHit == 0)
    {
        [[self parent] removeChild:self cleanup:YES];
        return;
    }

}


@end
