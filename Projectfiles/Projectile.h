//
//  Projectile.h
//  atatta
//
//  Created by Kevin Wong on 1/17/13.
//
//

#import "CCSprite.h"

@interface Projectile : CCSprite
{
    CGPoint velocity;
    float hitDelay;
    int maxHit;
    int damage;
    bool isInactive;
}

-(void) delayNextHit;

@end
