//
//  LevelMenu.m
//  atatta
//
//  Created by Kevin Wong on 1/16/13.
//
//

#import "LevelMenu.h"
#import "Level.h"
#import "SimpleAudioEngine.h"

#define MAX_LEVEL 3

@interface LevelMenu (PrivateMethods)

@end

@implementation LevelMenu

static LevelMenu * instanceOfLevelMenu;


-(id) init
{
	if ((self = [super init]))
	{
        instanceOfLevelMenu = self;
        
        [self initUserData];
        
        myMenu = [CCMenu menuWithItems:nil];
        [myMenu setPosition:ccp(240,160)];
                
        sky = [CCSprite spriteWithFile:@"sky.jpeg"];
        [sky setAnchorPoint:ccp(0,0)];
        [sky setPosition:ccp(0,0)];
        
        selectLevelText = [CCSprite spriteWithFile:@"select-level.png"];
        [selectLevelText setScale:.6];
        [selectLevelText setAnchorPoint:ccp(.5,.5)];
        [selectLevelText setPosition:ccp(240,280)];
        
        [self resetLevelMenu];
	}
    return self;
}

-(void) resetLevelMenu
{
    int currentLevel = [[[NSUserDefaults standardUserDefaults] objectForKey:@"currentPlayableLevel"] integerValue];
    
    // remove all menu buttons:
    [myMenu removeAllChildrenWithCleanup:YES];
    [self removeAllChildrenWithCleanup:NO];
    
    [self addChild:myMenu z:0];
    [self addChild:sky z:-100];
    [self addChild:selectLevelText z:1];

    
    for (int i = 1; i <= MAX_LEVEL; i++)
    {
        CCMenuItemImage * playLevelButton;
        CGPoint centerPosition = [self getMenuItemPosition:i];
        
        if (i <= currentLevel)
        {
            // level completed
            playLevelButton = [CCMenuItemImage itemWithNormalImage:@"hexagon.png"
                                                     selectedImage:@"hexagon-pressed.png"
                                                            target:self
                                                          selector:@selector(playLevel:)];
        }
        else
        {
            playLevelButton = [CCMenuItemImage itemWithNormalImage:@"hexagon-grey.png"
                                                     selectedImage:@"hexagon-grey.png"
                                                            target:self
                                                          selector:@selector(doNothing)];
        }
        
        if (i < currentLevel)
        {
            CCSprite * check = [CCSprite spriteWithFile:@"check.png"];
            [check setScale:.2];
            [check setAnchorPoint:ccp(0,.8)];
            [check setPosition:ccpAdd(centerPosition, [myMenu position])];
            [self addChild:check];
        }
        
        [playLevelButton setAnchorPoint:ccp(.5,.5)];
        [playLevelButton setTag:i];
        [playLevelButton setScale:0.2];
        [playLevelButton setPosition:centerPosition];
        [myMenu addChild:playLevelButton];
        
        // add the number
        CCLabelTTF * number = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%i", i]
                                                 fontName:@"marker felt"
                                                 fontSize:25];
        [number setAnchorPoint:ccp(.5,.5)];
        [number setColor:ccBLACK];
        [number setPosition:ccpAdd(centerPosition, [myMenu position])];
        [self addChild:number];
    }
    // disable tap (not sure why this have to be done)
    KKInput * input = [KKInput sharedInput];
    input.gestureTapEnabled = NO;
    numberInitialized = true;
}

-(void) playLevel: (CCMenuItem *) menuItem;
{
    int parameter = menuItem.tag;
    [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
    [[CCDirector sharedDirector] replaceScene:[Level sceneLevel:parameter]];
}


+(id) scene
{
    if ([[SimpleAudioEngine sharedEngine] isBackgroundMusicPlaying])
    {
        [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
    }
    [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"POL-air-ducts-short.wav" loop:YES];

    if (instanceOfLevelMenu == nil)
    {
        instanceOfLevelMenu = [[LevelMenu alloc] init];
    }
    [instanceOfLevelMenu resetLevelMenu];
    
    CCScene *scene = [CCScene node];
	[scene addChild: instanceOfLevelMenu];
	return scene;
}

-(void) initUserData
{
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"levelDataInitialized"])
    {
        // playing game for the first time        
        [[NSUserDefaults standardUserDefaults] setObject:@1 forKey:@"currentPlayableLevel"];
        
        // already initialized:
        [[NSUserDefaults standardUserDefaults] setObject:@true forKey:@"levelDataInitialized"];
    }
}

-(void) levelWon: (int) level
{
    int currentLevel = [[[NSUserDefaults standardUserDefaults] objectForKey:@"currentPlayableLevel"] integerValue];

    if (level == currentLevel)
    {
        [[NSUserDefaults standardUserDefaults] setObject:@(currentLevel+1) forKey:@"currentPlayableLevel"];
    }
}

-(void) doNothing
{
}

-(CGPoint) getMenuItemPosition: (int) i
{
    int x,y;
        
    if (i <=3)
    {
        y = 50;
    }
    else
    {
        y = -50;
    }
    
    x = 100*((i-1)%3 -1);
    
    return ccp(x,y);
}

+(LevelMenu*) sharedLevelMenu
{
    return instanceOfLevelMenu;
}



@end
