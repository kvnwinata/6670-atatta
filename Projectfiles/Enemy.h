//
//  Enemy.h
//  atatta
//
//  Created by Kevin Wong on 1/17/13.
//
//

#import "CCSprite.h"
#import "Level.h"

@interface Enemy : CCSprite
{
    int health;
    int moneyDrop;
    CGPoint velocity;
    CGPoint attackingPosition;
    
    CCAction *animation;
    NSMutableArray *animationFrames;
    
    LevelEvent event;
    bool isAttacking;
    bool canAttack;
    bool canThrow; 
    
    CCSprite * slash;

}

-(id) initWithEvent: (LevelEvent) event;

-(void) update: (ccTime) dt;
-(bool) isIncludingPoint: (CGPoint) location;
-(void) takeDamage: (int) damage;

-(void) resetAttacking;

@end
