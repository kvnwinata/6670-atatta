//
//  SmallBird.h
//  atatta
//
//  Created by Kevin Wong on 2/1/13.
//
//

#import "Enemy.h"

@interface SmallBird : Enemy
{
    ccTime t;
}
-(void) updatePosition: (ccTime) dt;

@end
