//
//  Archer.h
//  atatta
//
//  Created by Kevin Wong on 1/23/13.
//
//

#import "Defender.h"

@interface Archer : Defender
{
    CGPoint startLocation;
    CGPoint endLocation;
    
    CCSprite * directionalArrow;
    CCSprite * bow;
}

-(id) initWithSlotPosition: (int) slot_position;
-(void) update: (ccTime) dt;


@end
