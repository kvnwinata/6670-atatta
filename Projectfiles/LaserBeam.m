//
//  LaserBeam.m
//  atatta
//
//  Created by Kevin Wong on 2/1/13.
//
//

#import "LaserBeam.h"
#import "Level.h"
#import "Enemy.h"
#import "SimpleAudioEngine.h"

@implementation LaserBeam

-(id) initWithPosition: (CGPoint) pos
              andAngle: (float) angle
{
    if (self = [super initWithFile:@"laser-beam.png"])
    {
        damage = 50;
        
        [self setAnchorPoint:ccp(0,.5)];
        [self setScaleY:0];
        [self setPosition:pos];
        [self setRotation:angle];
        
        [self runAction:[CCScaleTo actionWithDuration:.3 scaleX:1 scaleY:.5]];
        [self performSelector:@selector(attack) withObject:nil afterDelay:.4];
    }
    return self;
}

-(void) attack
{
 
    CCArray * enemies = [[Level sharedCurrentLevel] enemiesManager].children.copy;
    
    for(unsigned i = 0; i < enemies.count; i++)
    {
        Enemy * enemy = [enemies objectAtIndex:i];
        if ([self isIntersecting:enemy])
        {
            [enemy takeDamage:damage];
            [[SimpleAudioEngine sharedEngine] playEffect:@"Pow.caf"];
        }
    }
    
    [self performSelector:@selector(shrink) withObject:nil afterDelay:.5];
}

-(void) shrink
{
    float delay = .5;
    [self runAction:[CCScaleTo actionWithDuration:delay scaleX:1 scaleY:0]];
    [self performSelector:@selector(removeSelf) withObject:nil afterDelay:delay];
}

-(void) removeSelf
{
    [[self parent] removeChild:self cleanup:YES];
}

-(bool) isIntersecting: (Enemy *) enemy
{
    return CGRectContainsPoint(self.boundingBox, enemy.position);
    
}


@end
