//
//  Enemy.m
//  atatta
//
//  Created by Kevin Wong on 1/17/13.
//
//

#import "Enemy.h"
#import "Rock.h"

#import "UtilityMethods.h"
#import "SimpleAudioEngine.h"

@implementation Enemy

-(void) update: (ccTime) dt
{
    if (!isAttacking)
    {
        [self updatePosition:dt];  
    }
    
    //check if hit a defender:
    for(Defender * defender in [[Level sharedCurrentLevel] defendersManager].children)
    {
        if ([defender isIncludingPoint:ccpAdd(ccp(self.position.x,0), attackingPosition)])
        {
            isAttacking = true;
            
            if (canAttack){
                [defender takeDamage:10];
                [[SimpleAudioEngine sharedEngine] playEffect:@"eat.m4a"];
                canAttack = false;
                [self performSelector:@selector(canAttackReset) withObject:nil afterDelay:1];
            }
        }
    }    
    
    // level is lost if position is < 0
    if (self.position.x < 0){
        [[Level sharedCurrentLevel] activateEvent:lost];
    }
    
    if (canThrow){
        Rock * rock = [[Rock alloc] initAtPosition:ccpAdd(ccp(self.position.x,0), attackingPosition)];
        [self performSelector:@selector(canThrowReset) withObject:nil afterDelay:3];
        [[[Level sharedCurrentLevel] projectilesManager] addChild:rock];
        canThrow = false;
    }
}

-(void) updatePosition: (ccTime) dt
{
    // update current position
    CGPoint currentPosition = [self position];
    CGPoint newPosition = ccp(currentPosition.x + velocity.x*dt, currentPosition.y + velocity.y*dt);
    [self setPosition:newPosition];
}

-(bool) isIncludingPoint:(CGPoint)location
{
    return CGRectContainsPoint(self.boundingBox, location);
}

-(void) takeDamage:(int) amount
{
    [[[Level sharedCurrentLevel] storeManager] incrementCombo];
    health -= amount;
    isAttacking = false;
    
    if (health <= 0)
    {
        if (event != 0)
        {
            [[Level sharedCurrentLevel] activateEvent: event];
        }
        [[[Level sharedCurrentLevel] storeManager] incrementMoney:moneyDrop
                                                         position:ccpAdd(ccp(self.position.x,0), attackingPosition)];
        [[self parent] removeChild:self cleanup:NO];
    } else {
        [self runAction:[CCSequence actions:
                         [CCTintTo actionWithDuration:.05 red:255 green:255 blue:255],
                         [CCTintTo actionWithDuration:.05 red:100 green:100 blue:100],
                         [CCTintTo actionWithDuration:.05 red:255 green:255 blue:255],
                         [CCTintTo actionWithDuration:.05 red:100 green:100 blue:100],
                         [CCTintTo actionWithDuration:.05 red:255 green:255 blue:255],
                         [CCTintTo actionWithDuration:.05 red:100 green:100 blue:100],
                         [CCTintTo actionWithDuration:.05 red:255 green:255 blue:255],
                         nil]];
        [self runAction:[CCJumpTo actionWithDuration:.5
                                            position:ccpAdd(self.position, ccp(15,0))
                                              height:5 jumps:1]];
    }
}

-(id) initWithEvent: (LevelEvent) _event
{
    event = _event;
    canAttack = true;
    return [self init];
}

-(void) resetAttacking
{
    isAttacking = false;
}

-(void) canAttackReset
{
    canAttack = true;
}

-(void) canThrowReset
{
    canThrow = true;
}

@end
