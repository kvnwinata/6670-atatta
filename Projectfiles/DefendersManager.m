//
//  DefendersManager.m
//  atatta
//
//  Created by Kevin Wong on 1/22/13.
//
//

#import "DefendersManager.h"
#import "Level.h"
#import "HeavyArcher.h"
#import "LaserBee.h"
#import "MissileBee.h"

@implementation DefendersManager

@synthesize availableDefenderSlot;

-(id) init
{
    if (self = [super init])
    {
        [self cacheAnimationSprites];

    }
    return self;
}

-(void) cacheAnimationSprites
{
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:
     @"yellowbee.plist"];
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:
     @"wingbee.plist"];
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:
     @"redbee.plist"];
}


-(void) resetManager
{
    availableDefenderSlot = 0;
    int currentLevel = [[Level sharedCurrentLevel] level];    
}

-(void) clearManager
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [self removeAllChildrenWithCleanup:YES];
    
    // level always start empty
    availableDefenderSlot = 0;
    [[[Level sharedCurrentLevel] storeManager] updatePlacementGuide:availableDefenderSlot];
}

-(bool) slot: (int) slot
 isTouchedAt: (CGPoint) pos
{
    int x = pos.x;
    int y = pos.y;
    
    if (x > 0 && x < DEFENDER_SIZE)
    {
        float touchCoord = (y - GROUND_HEIGHT)/(float)DEFENDER_SIZE;
        
        if (slot < touchCoord && touchCoord < slot + 1)
        {
            return true;
        }
    }
    return false;
}

-(void) addNewDefender: (Defender *) newDefender
{
    [self addChild:newDefender];
    availableDefenderSlot ++;
    [[[Level sharedCurrentLevel] storeManager] updatePlacementGuide:availableDefenderSlot];
}

-(void) removeDefender: (Defender *) defender slot:(int) slotPosition
{
    if([[Level sharedCurrentLevel] lastObjectTouched] == defender)
    {
        [Level sharedCurrentLevel].lastObjectTouched = 0;
    }
    [self removeChild:defender cleanup:YES];
    
    for (Defender * def in self.children)
    {
        [def updateSlotPosition: slotPosition];
    }
    availableDefenderSlot--;
    [[[Level sharedCurrentLevel] storeManager] updatePlacementGuide:availableDefenderSlot];

    
    // reset the status of all attacking enemies
    [[[Level sharedCurrentLevel] enemiesManager] resetAttackingEnemies];
    
}


@end
