//
//  Rock.h
//  atatta
//
//  Created by Kevin Wong on 2/1/13.
//
//

#import "CCSprite.h"

@interface Rock : CCSprite

-(id) initAtPosition:(CGPoint) pos;

-(void) update: (ccTime) dt;

@end
