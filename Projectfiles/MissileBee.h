//
//  MissileBee.h
//  atatta
//
//  Created by Kevin Wong on 2/1/13.
//
//

#import "Defender.h"

@interface MissileBee : Defender
{
    CGPoint startLocation;
    CGPoint endLocation;
    
    CCSprite * directionalArrow;
    CCSprite * launcher;
}
-(id) initWithSlotPosition: (int) slot_position;
-(void) update: (ccTime) dt;

@end
