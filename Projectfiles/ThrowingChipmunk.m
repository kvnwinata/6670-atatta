//
//  ThrowingChipmunk.m
//  atatta
//
//  Created by Kevin Wong on 2/1/13.
//
//

#import "ThrowingChipmunk.h"

@implementation ThrowingChipmunk

-(id) init
{
 	if ((self = [super initWithSpriteFrameName:@"chipmunk1.tiff"]))
	{
        health = 40;
        velocity = ccp(-40,0);
        moneyDrop = 20;
        
        attackingPosition = ccp(0,GROUND_HEIGHT+DEFENDER_SIZE/2);
        
        [self setScale:.4];
        [self setAnchorPoint:ccp(0.5, 0)];
        [self setPosition:ccp(480 + 100, GROUND_HEIGHT)];
        
        // setup animation
        animationFrames = [NSMutableArray array];
        for(int i = 1; i <= 6; ++i)
        {
            [animationFrames addObject:
             [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName: [NSString stringWithFormat:@"chipmunk%d.tiff", i]]];
        }
        CCAnimation *animating = [CCAnimation animationWithFrames: animationFrames delay:0.1f];
        animation = [CCRepeatForever actionWithAction: [CCAnimate actionWithAnimation:animating restoreOriginalFrame:NO]];
        [self runAction:animation];
        
        // schedule update: using superclass's update:
        [self scheduleUpdate];
        
        canThrow = true;
	}
    return self;
}

@end
