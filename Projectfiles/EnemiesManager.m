//
//  EnemiesManager.m
//  atatta
//
//  Created by Kevin Wong on 1/22/13.
//
//

#import "EnemiesManager.h"
#import "Enemy.h"
#import "Level.h"

@implementation EnemiesManager

-(id) init
{
    if (self = [super init])
    {
        [self cacheAnimationSprites];
    }
    return self;
}

-(void) cacheAnimationSprites
{
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:
     @"chipmunk.plist"];
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:
     @"pig.plist"];
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:
     @"turtle.plist"];
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:
     @"stork.plist"];
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:
     @"smallbird.plist"];
}

-(void) resetManager
{
    int currentLevel = [[Level sharedCurrentLevel] level];
    currentWave = 0;
        
    // load the enemies
    NSString *path = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"enemiesLevel%i", currentLevel] ofType:@"plist"];
    enemiesWave = [NSArray arrayWithContentsOfFile:path];
}

-(void) clearManager
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [self removeAllChildrenWithCleanup:YES];
}

-(void) spawnNextWave
{
    NSArray * wave = [enemiesWave objectAtIndex:currentWave];
    
    for (NSDictionary * enemy_dict in wave)
    {
        // retrieve event that gets activated when enemy is killed
        LevelEvent event = 0;
        if ([enemy_dict valueForKey:@"event"] != nil)
        {
            event = (LevelEvent)[[enemy_dict valueForKey:@"event"] integerValue];
        }
        
        // initialize the enemy
        NSString * type = [enemy_dict valueForKey:@"type"];
        Enemy * enemy = [[NSClassFromString(type) alloc] initWithEvent:event];

        // spawn the enemy after a certain delay
        int delay = [[enemy_dict valueForKey:@"delay"] floatValue];
        [self performSelector:@selector(spawnEnemy:) withObject:enemy afterDelay:delay];
    }
    currentWave++;
}

-(void) spawnEnemy: (Enemy*) enemy
{
    [self addChild:enemy];
}

-(void) resetAttackingEnemies
{
    for (Enemy * enemy in self.children)
    {
        [enemy resetAttacking];
    }
}
@end
