//
//  TutorialsManager.h
//  atatta
//
//  Created by Kevin Wong on 1/22/13.
//
//

#import "CCLayer.h"

typedef enum
{
    victory = 1,
    lost = 2,
    spawn_wave = 3,
    next_tutorial = 4,
} LevelEvent;


@interface TutorialsManager : CCLayer
{
    int currentSegment;
    NSArray * tutorialSegments;
    
    CCLabelTTF * text;
    
    CCSprite * textHolder;
    
    CCSprite * bee;
    CCSprite * arrow;
    CCSprite * line;
    
    CCAction * animation;
    
    bool isWaitingTapText;
    bool updateScheduled;

    LevelEvent nextEvent;
}

-(void) resetManager;
-(void) clearManager;

-(void) nextSegment;
-(void) update: (ccTime) dt;

-(void) rerunScheduledActions;


@end
