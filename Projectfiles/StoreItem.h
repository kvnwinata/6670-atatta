//
//  StoreItem.h
//  atatta
//
//  Created by Kevin Wong on 1/29/13.
//
//

#import "CCSprite.h"
#import "Defender.h"

@interface StoreItem : CCSprite
{
    int itemSlotPosition;
    int availableDefenderSlot;
    
    int price;
    
    bool touched;
    bool purchasable;
}

-(id) initWithSlotPosition: (int) slot_position;

-(bool) isTouchedAtPosition: (CGPoint) pos;
-(void) updatePurchasability: (int) current_money;

-(Defender*) createDefenderAtSlot: (int) slot;

-(CGPoint) getCoordFromSlotPosition;

-(void) updateAvailableDefenderSlot: (int) slot;

@end
