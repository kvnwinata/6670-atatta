//
//  MainMenu.m
//  atatta
//
//  Created by Kevin Wong on 1/16/13.
//
//

#import "MainMenu.h"
#import "LevelMenu.h"
#import "SimpleAudioEngine.h"

#import "Archer.h"

@interface MainMenu (PrivateMethods)
-(void)startPlay;
@end

@implementation MainMenu

-(id) init
{
	if ((self = [super init]))
	{
        
        CCMenu * myMenu = [CCMenu menuWithItems:nil];
        [myMenu setPosition:ccp(240,160)];

        // add play button:
        CCMenuItemImage * playButton = [CCMenuItemImage itemWithNormalImage:@"play.png"
                                                              selectedImage:@"play-pressed.png"
                                                                     target:self
                                                                   selector:@selector(startPlay)];
        [playButton setPosition:ccp(playButton.position.x, playButton.position.y-60)];
        [playButton setScale:.5];
        [myMenu addChild:playButton];
        [self addChild:myMenu];
        
        [[SimpleAudioEngine sharedEngine] preloadEffect:@"Pow.caf"];
        [[SimpleAudioEngine sharedEngine] preloadBackgroundMusic:@"POL-air-ducts-short.wav"];

        [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"POL-air-ducts-short.wav" loop:YES];
        
        CCSprite * sprite = [CCSprite spriteWithFile:@"sky.jpeg"];
        [sprite setAnchorPoint:ccp(0,0)];
        [sprite setPosition:ccp(0,0)];
        [self addChild:sprite z:-100];
        
        sprite = [CCSprite spriteWithFile:@"title.png"];
        [sprite setAnchorPoint:ccp(.5,.5)];
        [sprite setPosition:ccp(240,200)];
        [self addChild:sprite z:-99];
        
	}
    return self;
}


-(void) startPlay
{
    [[CCDirector sharedDirector] replaceScene:[LevelMenu scene]];
}

+(id) scene
{
    CCScene *scene = [CCScene node];
	MainMenu *layer = [MainMenu node];
	[scene addChild: layer];
	return scene;
}
@end
