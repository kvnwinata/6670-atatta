//
//  NormalStork.m
//  atatta
//
//  Created by Kevin Wong on 1/30/13.
//
//

#import "NormalStork.h"

@implementation NormalStork


-(id) init
{
 	if ((self = [super initWithSpriteFrameName:@"stork1.tiff"]))
	{
        health = 30;
        velocity = ccp(-20,0);
        moneyDrop = 20;
        
        canThrow = true;
        
        [self setScale:1];
        [self setAnchorPoint:ccp(.5,.3)];
        [self setFlipX:YES];
        
        int rndValue = GROUND_HEIGHT*2 + arc4random() % (SCREEN_HEIGHT - GROUND_HEIGHT*2);

        [self setPosition:ccp(480 + 100, rndValue)];
        
        attackingPosition = ccp(0,rndValue+25);
        
        // setup animation
        animationFrames = [NSMutableArray array];
        for(int i = 1; i <= 12; ++i)
        {
            [animationFrames addObject:
             [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName: [NSString stringWithFormat:@"stork%d.tiff", i]]];
        }
        CCAnimation *animating = [CCAnimation animationWithFrames: animationFrames delay:0.3f];
        animation = [CCRepeatForever actionWithAction: [CCAnimate actionWithAnimation:animating restoreOriginalFrame:NO]];
        [self runAction:animation];
        
        // schedule update: using superclass's update:
        [self scheduleUpdate];

	}
    return self;
}

@end
