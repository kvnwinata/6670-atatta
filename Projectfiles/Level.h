//
//  Level.h
//  atatta
//
//  Created by Kevin Wong on 1/17/13.
//
//

#import "CCLayer.h"
#import "EnemiesManager.h"
#import "DefendersManager.h"
#import "TutorialsManager.h"
#import "DefenderStoreManager.h"
#import "ProjectilesManager.h"
#import "SimpleAudioEngine.h"

#define GROUND_HEIGHT 60
#define SCREEN_WIDTH 480
#define SCREEN_HEIGHT 320
#define DEFENDER_SIZE 65

#define STORE_ITEM_WIDTH 120

@interface Level : CCLayer
{
    CCLabelTTF * levelNumber;
    CCSprite * rumbleText;
    CCSprite * line;
    
    CCRepeatForever * lineAnimation;
    bool lineAnimationRunning;
    
    CCSprite * victoryText;
    CCSprite * defeatText;
    CCMenu * defeatMenu;
}

@property int level;
@property EnemiesManager * enemiesManager;
@property DefendersManager * defendersManager;
@property TutorialsManager * tutorialsManager;
@property DefenderStoreManager * storeManager;
@property ProjectilesManager * projectilesManager;
@property id lastObjectTouched;

+(Level*) sharedCurrentLevel;

-(void) draw;

+(id) sceneLevel: (int) level;

-(void) activateEvent: (LevelEvent) event;
-(void) animateLine: (bool) animate;

@end
