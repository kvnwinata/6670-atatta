//
//  ArcherStoreItem.m
//  atatta
//
//  Created by Kevin Wong on 1/29/13.
//
//

#import "ArcherStoreItem.h"
#import "Archer.h"

@implementation ArcherStoreItem

-(id) init
{
    if ((self = [super initWithFile:@"archer-store.png"]))
	{
        // setup the variables:
        price = 100;
        
        [self setAnchorPoint:ccp(.5,.5)];
        [self setPosition:[self getCoordFromSlotPosition]];

        // resize the store item
        [self setScale:.25];
        
        [self scheduleUpdate];
	}
    return self;
}

-(Defender*) createDefenderAtSlot: (int) slot
{
    return [[Archer alloc] initWithSlotPosition:slot];
}

@end
