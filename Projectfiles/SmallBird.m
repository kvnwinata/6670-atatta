//
//  SmallBird.m
//  atatta
//
//  Created by Kevin Wong on 2/1/13.
//
//

#import "SmallBird.h"

@implementation SmallBird

-(id) init
{
 	if ((self = [super initWithSpriteFrameName:@"smallbird1.tiff"]))
	{
        health = 20;
        velocity = ccp(-40,0);
        moneyDrop = 10;
        
        canThrow = true;
        
        [self setScale:.5];
        [self setAnchorPoint:ccp(.5,.3)];
        [self setFlipX:YES];
        
        int rndValue = GROUND_HEIGHT*2 + arc4random() % (SCREEN_HEIGHT - GROUND_HEIGHT*2);
        
        [self setPosition:ccp(480 + 100, rndValue)];
        
        attackingPosition = ccp(0,rndValue);
        
        // setup animation
        animationFrames = [NSMutableArray array];
        for(int i = 1; i <= 3; ++i)
        {
            [animationFrames addObject:
             [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName: [NSString stringWithFormat:@"smallbird%d.tiff", i]]];
        }
        CCAnimation *animating = [CCAnimation animationWithFrames: animationFrames delay:0.3f];
        animation = [CCRepeatForever actionWithAction: [CCAnimate actionWithAnimation:animating restoreOriginalFrame:NO]];
        [self runAction:animation];
        
        // schedule update: using superclass's update:
        [self scheduleUpdate];
        t = 0;
        
	}
    return self;
}

-(void) updatePosition: (ccTime) dt
{
    t+= dt;
    // update current position
    velocity.x = -30-100*sin(t);
    velocity.y = -50*sin(t);
    
    CGPoint currentPosition = [self position];
    CGPoint newPosition = ccp(currentPosition.x + velocity.x*dt, currentPosition.y + velocity.y*dt);
    
    attackingPosition = ccp(0, newPosition.y);
    
    [self setPosition:newPosition];
}


@end
