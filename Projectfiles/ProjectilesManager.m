//
//  ProjectilesManager.m
//  atatta
//
//  Created by Kevin Wong on 1/23/13.
//
//

#import "ProjectilesManager.h"
#import "Level.h"

@implementation ProjectilesManager


-(void) resetManager
{
    int currentLevel = [[Level sharedCurrentLevel] level];
}


-(void) clearManager
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [self removeAllChildrenWithCleanup:YES];
}

@end
