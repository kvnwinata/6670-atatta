//
//  ArcherStoreItem.h
//  atatta
//
//  Created by Kevin Wong on 1/29/13.
//
//

#import "StoreItem.h"

@interface ArcherStoreItem : StoreItem

-(id) init;
-(Defender*) createDefenderAtSlot: (int) slot;

@end
